package tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Keys;
import pages.CardPayment;
import pages.CardPaymentResult;

import java.util.Calendar;

public class CardPaymentFormTest extends FunctionalTest {
    private String url = "https://sandbox.cardpay.com/MI/cardpayment2.html?orderXml=PE9SREVSIFdBTExFVF9JRD0nODI5OScgT1JERVJfTlVNQkVSPSc0NTgyMTEnIEFNT1VOVD0nMjkxLjg2JyAgRU1BSUw9J2N1c3RvbWVyQGV4YW1wbGUuY29tJz4KPEFERFJFU1MgQ09VTlRSWT0nVVNBJyBTVEFURT0nTlknIFpJUD0nMTAwMDEnIENJVFk9J05ZJyBTVFJFRVQ9JzY3NyBTVFJFRVQnIFBIT05FPSc4NzY5OTA5MCcgVFlQRT0nQklMTElORycvPgo8L09SREVSPg==&sha512=529477e3f6966a47272add0ca4d552bc8e83f27c1682c87abfcb38caa026acb4d5bd3b97dfeee60b02407e85eaf62dbd0f4add7bbf74320bc26c4b9db56722ba";
    private String cardNumber;
    private String orderNumberDataText;
    private String fourLastCardCharacters;
    private String totalAmmount;
    private String cardHolder;
    private String cardExpiredMonth;
    private String cardExpiredYear;
    private String cardNumberIsRequired = "Card number is required";
    private String cardholderNameIsRequired = "Cardholder name is required";
    private String expirationDateIsRequired = "Expiration Date is required";
    private String cvv2IsRequired = "CVV2 is required";
    private String cardNumberIsNotValid = "Card number is not valid";
    private String cardholderNameIsNotValid = "Cardholder name is not valid.";
    private String invalidDataProvided = "Invalid data provided";
    private String cvv2IsNotValid = "CVV2 is not valid";
    private String paymentStatusAPPROVED = "APPROVED";
    private String paymentStatusDECLINED = "DECLINED";
    private String paymentStatusPENDING = "PENDING";
    private String cardTypeVISA = "VISA";
    private String cardTypeMASTERCARD = "MASTERCARD";

    private Calendar calendar = Calendar.getInstance();


    @Test
    public void view() { //Проверка отображения
        driver.get(url);
        CardPayment cardPayment = new CardPayment(driver);

        //Наименований заголовка
        Assert.assertEquals(cardPayment.getMerchantTitle(), "cardpay_test.com");
        Assert.assertEquals(cardPayment.getOrderNumber(), "458211");
        Assert.assertEquals(cardPayment.getTotalAmmount(), "291.86");
        Assert.assertEquals(cardPayment.getTotalAmmountCurrency(), "EUR");

        //Отображения полей
        Assert.assertTrue(cardPayment.cardNumberFieldVisible());
        Assert.assertTrue(cardPayment.cardHolderFieldVisible());
        Assert.assertTrue(cardPayment.cardExpiredFieldVisible());
        Assert.assertTrue(cardPayment.cardCVCFieldVisible());
        Assert.assertTrue(cardPayment.paymentActionsVisible());
    }

    @Test
    public void payWithNullData() { //Пустые поля
        driver.get(url);
        CardPayment cardPayment = new CardPayment(driver);

        cardPayment.payButtonClick();

        //Ошибки
        Assert.assertEquals(cardPayment.getCardNumberFieldErrorText(), cardNumberIsRequired);
        Assert.assertEquals(cardPayment.getCardHolderFieldErrorText(), cardholderNameIsRequired);
        Assert.assertEquals(cardPayment.getCardExpiredFieldErrorText(), expirationDateIsRequired);
        Assert.assertEquals(cardPayment.getCardCVCFieldErrorText(), cvv2IsRequired);
    }

    @Test
    public void payWithNoValidDataWithCancel() { //Не валидные значения
        driver.get(url);
        CardPayment cardPayment = new CardPayment(driver);

        cardExpiredMonth = "01";
        cardExpiredYear = Integer.toString(calendar.get(Calendar.YEAR));
        cardHolder = "123";

        //Не вводится текст
        cardNumber = "visa";
        cardPayment.cardNumberFieldClick();
        cardPayment.cardNumberFieldSendKeys(cardNumber);
        Assert.assertEquals(cardPayment.getCardNumberFieldText(), "");

        //Введен не валидный номер + табуляция
        cardNumber = "1234";
        cardPayment.cardNumberFieldClick();
        cardPayment.cardNumberFieldSendKeys(cardNumber);
        cardPayment.cardNumberFieldSendKeys(Keys.TAB);
        Assert.assertEquals(cardPayment.getCardNumberFieldErrorText(), cardNumberIsNotValid);

        //Введен не валидный владелец + табуляция
        cardPayment.cardHolderFieldClick();
        cardPayment.cardHolderFieldSendKeys(cardHolder);
        cardPayment.cardHolderFieldSendKeys(Keys.TAB);
        Assert.assertEquals(cardPayment.getCardHolderFieldErrorText(), cardholderNameIsNotValid);

        //Введена не валидная дата + табуляция
        cardPayment.cardExpiredMonthSelectByVisibleText(cardExpiredMonth);
        cardPayment.cardExpiredMonthFieldSendKeys(Keys.TAB);
        cardPayment.cardExpiredYearSelectByVisibleText(cardExpiredYear);
        cardPayment.cardExpiredYearFieldSendKeys(Keys.TAB);
        Assert.assertEquals(cardPayment.getCardExpiredFieldErrorText(), invalidDataProvided);

        //Введен не валидный код CVV2 + табуляция
        cardPayment.inputCardCVCFieldClick();
        cardPayment.inputCardCVCFieldSendKeys("12");
        cardPayment.inputCardCVCFieldSendKeys(Keys.TAB);
        Assert.assertEquals(cardPayment.getCardCVCFieldErrorText(), cvv2IsNotValid);

        cardPayment.payButtonClick();

        //Ошибки после попытки оплатить
        Assert.assertEquals(cardPayment.getCardNumberFieldErrorText(), cardNumberIsNotValid);
        Assert.assertEquals(cardPayment.getCardHolderFieldErrorText(), cardholderNameIsNotValid);
        Assert.assertEquals(cardPayment.getCardExpiredFieldErrorText(), invalidDataProvided);
        Assert.assertEquals(cardPayment.getCardCVCFieldErrorText(), cvv2IsNotValid);

        //Отмена
        cardPayment.cancelButtonClick();
        Assert.assertTrue(!url.equals(driver.getCurrentUrl()));

    }

    @Test
    public void payComfirmedAs3D() {
        driver.get(url);
        CardPayment cardPayment = new CardPayment(driver);

        cardNumber = "4000000000000002";
        orderNumberDataText = cardPayment.getOrderNumber();
        fourLastCardCharacters = cardNumber.substring(cardNumber.length()-4);
        totalAmmount = cardPayment.getTotalAmmountCurrency() + "   " + cardPayment.getTotalAmmount();
        cardHolder = "BIG DATA";
        cardExpiredMonth = "0" + Integer.toString(calendar.get(Calendar.MONTH) + 1);
        cardExpiredYear = Integer.toString(calendar.get(Calendar.YEAR));

        cardPayment.cardNumberFieldClick();
        cardPayment.cardNumberFieldSendKeys(cardNumber);
        cardPayment.cardHolderFieldClick();
        cardPayment.cardHolderFieldSendKeys(cardHolder);
        cardPayment.cardExpiredMonthSelectByVisibleText(cardExpiredMonth);
        cardPayment.cardExpiredYearSelectByVisibleText(cardExpiredYear);
        cardPayment.inputCardCVCFieldClick();
        cardPayment.inputCardCVCFieldSendKeys("123");
        cardPayment.payButtonClick();

        driver.getPageSource();
        CardPaymentResult cardPaymentResult = new CardPaymentResult(driver);

        Assert.assertEquals(cardPaymentResult.getPaymentStatusDataText(), paymentStatusAPPROVED);
        Assert.assertEquals(cardPaymentResult.getOrderNumberDataText(), orderNumberDataText);
        Assert.assertNotNull(cardPaymentResult.getAuthorizationCodeDataText());
        Assert.assertEquals(cardPaymentResult.getCardNumberDataText(), "..." + fourLastCardCharacters);
        Assert.assertEquals(cardPaymentResult.getCardTypeDataText(), cardTypeVISA);
        Assert.assertEquals(cardPaymentResult.getCardHolderDataText(), cardHolder);
        Assert.assertEquals(cardPaymentResult.getTotalAmountDataText(), totalAmmount);
    }

    @Test
    public void payDeclinedAs3D() {
        driver.get(url);
        CardPayment cardPayment = new CardPayment(driver);

        cardNumber = "5555555555554444";
        orderNumberDataText = cardPayment.getOrderNumber();
        fourLastCardCharacters = cardNumber.substring(cardNumber.length()-4);
        totalAmmount = cardPayment.getTotalAmmountCurrency() + "   " + cardPayment.getTotalAmmount();
        cardHolder = "BIG DATA";
        cardExpiredMonth = "0" + Integer.toString(calendar.get(Calendar.MONTH) + 1);
        cardExpiredYear = Integer.toString(calendar.get(Calendar.YEAR) + 1);

        cardPayment.cardNumberFieldClick();
        cardPayment.cardNumberFieldSendKeys(cardNumber);
        cardPayment.cardHolderFieldClick();
        cardPayment.cardHolderFieldSendKeys(cardHolder);
        cardPayment.cardExpiredMonthSelectByVisibleText(cardExpiredMonth);
        cardPayment.cardExpiredYearSelectByVisibleText(cardExpiredYear);
        cardPayment.inputCardCVCFieldClick();
        cardPayment.inputCardCVCFieldSendKeys("234");
        cardPayment.payButtonClick();

        driver.getPageSource();
        CardPaymentResult cardPaymentResult = new CardPaymentResult(driver);

        Assert.assertEquals(cardPaymentResult.getPaymentStatusDataText(), paymentStatusDECLINED);
        Assert.assertEquals(cardPaymentResult.getOrderNumberDataText(), orderNumberDataText);
        Assert.assertEquals(cardPaymentResult.getCardNumberDataText(), "..." + fourLastCardCharacters);
        Assert.assertEquals(cardPaymentResult.getCardTypeDataText(), cardTypeMASTERCARD);
        Assert.assertEquals(cardPaymentResult.getCardHolderDataText(), cardHolder);
        Assert.assertEquals(cardPaymentResult.getTotalAmountDataText(), totalAmmount);
    }

    @Test
    public void payPendingAs3D() {
        driver.get(url);
        CardPayment cardPayment = new CardPayment(driver);

        cardNumber = "4000000000000044";
        orderNumberDataText = cardPayment.getOrderNumber();
        fourLastCardCharacters = cardNumber.substring(cardNumber.length()-4);
        totalAmmount = cardPayment.getTotalAmmountCurrency() + "   " + cardPayment.getTotalAmmount();
        cardHolder = "BIG DATA";
        cardExpiredMonth = "0" + Integer.toString(calendar.get(Calendar.MONTH) + 2);
        cardExpiredYear = Integer.toString(calendar.get(Calendar.YEAR) + 2);

        cardPayment.cardNumberFieldClick();
        cardPayment.cardNumberFieldSendKeys(cardNumber);
        cardPayment.cardHolderFieldClick();
        cardPayment.cardHolderFieldSendKeys(cardHolder);
        cardPayment.cardExpiredMonthSelectByVisibleText(cardExpiredMonth);
        cardPayment.cardExpiredYearSelectByVisibleText(cardExpiredYear);
        cardPayment.inputCardCVCFieldClick();
        cardPayment.inputCardCVCFieldSendKeys("345");
        cardPayment.payButtonClick();

        driver.getPageSource();
        CardPaymentResult cardPaymentResult = new CardPaymentResult(driver);

        Assert.assertEquals(cardPaymentResult.getPaymentStatusDataText(), paymentStatusPENDING);
        Assert.assertEquals(cardPaymentResult.getOrderNumberDataText(), orderNumberDataText);
        Assert.assertNotNull(cardPaymentResult.getAuthorizationCodeDataText());
        Assert.assertEquals(cardPaymentResult.getCardNumberDataText(), "..." + fourLastCardCharacters);
        Assert.assertEquals(cardPaymentResult.getCardTypeDataText(), cardTypeVISA);
        Assert.assertEquals(cardPaymentResult.getCardHolderDataText(), cardHolder);
        Assert.assertEquals(cardPaymentResult.getTotalAmountDataText(), totalAmmount);
    }

    @Test
    public void payComfirmedAsNon3D() {
        driver.get(url);
        CardPayment cardPayment = new CardPayment(driver);

        cardNumber = "4000000000000077";
        orderNumberDataText = cardPayment.getOrderNumber();
        fourLastCardCharacters = cardNumber.substring(cardNumber.length()-4);
        totalAmmount = cardPayment.getTotalAmmountCurrency() + "   " + cardPayment.getTotalAmmount();
        cardHolder = "BIG DATA";
        cardExpiredMonth = "0" + Integer.toString(calendar.get(Calendar.MONTH) + 3);
        cardExpiredYear = Integer.toString(calendar.get(Calendar.YEAR) + 3);

        cardPayment.cardNumberFieldClick();
        cardPayment.cardNumberFieldSendKeys(cardNumber);
        cardPayment.cardHolderFieldClick();
        cardPayment.cardHolderFieldSendKeys(cardHolder);
        cardPayment.cardExpiredMonthSelectByVisibleText(cardExpiredMonth);
        cardPayment.cardExpiredYearSelectByVisibleText(cardExpiredYear);
        cardPayment.inputCardCVCFieldClick();
        cardPayment.inputCardCVCFieldSendKeys("456");
        cardPayment.payButtonClick();

        driver.getPageSource();
        CardPaymentResult cardPaymentResult = new CardPaymentResult(driver);

        Assert.assertEquals(cardPaymentResult.getPaymentStatusDataText(), paymentStatusAPPROVED);
        Assert.assertEquals(cardPaymentResult.getOrderNumberDataText(), orderNumberDataText);
        Assert.assertNotNull(cardPaymentResult.getAuthorizationCodeDataText());
        Assert.assertEquals(cardPaymentResult.getCardNumberDataText(), "..." + fourLastCardCharacters);
        Assert.assertEquals(cardPaymentResult.getCardTypeDataText(), cardTypeVISA);
        Assert.assertEquals(cardPaymentResult.getCardHolderDataText(), cardHolder);
        Assert.assertEquals(cardPaymentResult.getTotalAmountDataText(), totalAmmount);
    }

    @Test
    public void payDeclinedAsNon3D() {
        driver.get(url);
        CardPayment cardPayment = new CardPayment(driver);

        cardNumber = "5555555555554477";
        orderNumberDataText = cardPayment.getOrderNumber();
        fourLastCardCharacters = cardNumber.substring(cardNumber.length()-4);
        totalAmmount = cardPayment.getTotalAmmountCurrency() + "   " + cardPayment.getTotalAmmount();
        cardHolder = "BIG DATA";
        cardExpiredMonth = "0" + Integer.toString(calendar.get(Calendar.MONTH) + 4);
        cardExpiredYear = Integer.toString(calendar.get(Calendar.YEAR) + 4);

        cardPayment.cardNumberFieldClick();
        cardPayment.cardNumberFieldSendKeys(cardNumber);
        cardPayment.cardHolderFieldClick();
        cardPayment.cardHolderFieldSendKeys(cardHolder);
        cardPayment.cardExpiredMonthSelectByVisibleText(cardExpiredMonth);
        cardPayment.cardExpiredYearSelectByVisibleText(cardExpiredYear);
        cardPayment.inputCardCVCFieldClick();
        cardPayment.inputCardCVCFieldSendKeys("567");
        cardPayment.payButtonClick();

        driver.getPageSource();
        CardPaymentResult cardPaymentResult = new CardPaymentResult(driver);

        Assert.assertEquals(cardPaymentResult.getPaymentStatusDataText(), paymentStatusDECLINED);
        Assert.assertEquals(cardPaymentResult.getOrderNumberDataText(), orderNumberDataText);
        Assert.assertEquals(cardPaymentResult.getCardNumberDataText(), "..." + fourLastCardCharacters);
        Assert.assertEquals(cardPaymentResult.getCardTypeDataText(), cardTypeMASTERCARD);
        Assert.assertEquals(cardPaymentResult.getCardHolderDataText(), cardHolder);
        Assert.assertEquals(cardPaymentResult.getTotalAmountDataText(), totalAmmount);
    }

    @Test
    public void payPendingAsNon3D() {
        driver.get(url);
        CardPayment cardPayment = new CardPayment(driver);

        cardNumber = "4000000000000051";
        orderNumberDataText = cardPayment.getOrderNumber();
        fourLastCardCharacters = cardNumber.substring(cardNumber.length()-4);
        totalAmmount = cardPayment.getTotalAmmountCurrency() + "   " + cardPayment.getTotalAmmount();
        cardHolder = "BIG DATA";
        cardExpiredMonth = "0" + Integer.toString(calendar.get(Calendar.MONTH) + 5);
        cardExpiredYear = Integer.toString(calendar.get(Calendar.YEAR) + 5);

        cardPayment.cardNumberFieldClick();
        cardPayment.cardNumberFieldSendKeys(cardNumber);
        cardPayment.cardHolderFieldClick();
        cardPayment.cardHolderFieldSendKeys(cardHolder);
        cardPayment.cardExpiredMonthSelectByVisibleText(cardExpiredMonth);
        cardPayment.cardExpiredYearSelectByVisibleText(cardExpiredYear);
        cardPayment.inputCardCVCFieldClick();
        cardPayment.inputCardCVCFieldSendKeys("678");
        cardPayment.payButtonClick();

        driver.getPageSource();
        CardPaymentResult cardPaymentResult = new CardPaymentResult(driver);

        Assert.assertEquals(cardPaymentResult.getPaymentStatusDataText(), paymentStatusPENDING);
        Assert.assertEquals(cardPaymentResult.getOrderNumberDataText(), orderNumberDataText);
        Assert.assertNotNull(cardPaymentResult.getAuthorizationCodeDataText());
        Assert.assertEquals(cardPaymentResult.getCardNumberDataText(), "..." + fourLastCardCharacters);
        Assert.assertEquals(cardPaymentResult.getCardTypeDataText(), cardTypeVISA);
        Assert.assertEquals(cardPaymentResult.getCardHolderDataText(), cardHolder);
        Assert.assertEquals(cardPaymentResult.getTotalAmountDataText(), totalAmmount);
    }
}