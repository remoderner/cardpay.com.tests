package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class CardPayment extends Page {

    public CardPayment(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@id='merchant-title']")
    private WebElement merchantTitle; //Имя продавца

    @FindBy(xpath = "//div[@id='order-number-cnt']/span[@id='order-number']")
    private WebElement orderNumber; //Номер ордера

    @FindBy(xpath = "//div[@id='total-amount-cnt']/span[@id='total-amount']")
    private WebElement totalAmmount; //Общая сумма

    @FindBy(xpath = "//div[@id='total-amount-cnt']/span[@id='currency']")
    private WebElement totalAmmountCurrency; //Валюта

    @FindBy(xpath = "//fieldset[@id='card-number-field']")
    private WebElement cardNumberField; //Набор полей ввода номера карты

    @FindBy(xpath = "//input[@id='input-card-number']")
    private WebElement inputCardNumberField; //Поля для ввода карты

    @FindBy(xpath = "//fieldset[@id='card-holder-field']")
    private WebElement cardHolderField; //Набор полей ввода владельца карты

    @FindBy(xpath = "//input[@id='input-card-holder']")
    private WebElement inputCardHolderField; //Поля для ввода карты

    @FindBy(xpath = "//fieldset[@id='card-expires-field']")
    private WebElement cardExpiredField; //Набор полей ввода даты действия карты

    @FindBy(xpath = "//select[@id='card-expires-month']")
    private WebElement cardExpiredMonthField; //Дата действия карты, выпадающий список, месяцы

    @FindBy(xpath = "//select[@id='card-expires-year']")
    private WebElement cardExpiredYearField; //Дата действия карты, выпадающий список, годы

    @FindBy(xpath = "//fieldset[@id='card-cvc-field']")
    private WebElement cardCVCField; //Набор полей ввода кода CVV2 карты

    @FindBy(xpath = "//input[@id='input-card-cvc']")
    private WebElement inputCardCVCField; //Набор полей ввода кода CVV2 карты

    @FindBy(xpath = "//div[@id='payment-actions']")
    private WebElement paymentActions; //Действия по оплате

    @FindBy(xpath = "//input[@id='action-submit']")
    private WebElement payButton; //Кнопка pay

    @FindBy(xpath = "//button[@id='action-cancel']")
    private WebElement cancelButton; //Кнопка cancel

    /**
     * Common
     */
    private Select getSelect(WebElement webElement) { //Получить выпадающий список из веб элемента
        return new Select(webElement);
    }

    /**
     * Title
     */
    public String getMerchantTitle() {
        return merchantTitle.getText();
    }

    public String getOrderNumber() {
        return orderNumber.getText();
    }

    public String getTotalAmmount() {
        return totalAmmount.getText();
    }

    public String getTotalAmmountCurrency() {
        return totalAmmountCurrency.getText();
    }

    /**
     * cardNumber
     */
    public Boolean cardNumberFieldVisible() {
        return cardNumberField.isDisplayed();
    }

    public void cardNumberFieldClick() {
        inputCardNumberField.click();
    }

    public String getCardNumberFieldText() {
        return inputCardNumberField.getText();
    }

    public void cardNumberFieldSendKeys(Keys inputText) {
        inputCardNumberField.sendKeys(inputText);
    }

    public void cardNumberFieldSendKeys(String inputText) {
        inputCardNumberField.sendKeys(inputText);
    }

    public String getCardNumberFieldErrorText() {
        return cardNumberField.findElement(By.xpath(".//label[@class='error']")).getText();
    }

    /**
     * cardHolder
     */
    public Boolean cardHolderFieldVisible() {
        return cardHolderField.isDisplayed();
    }

    public void cardHolderFieldClick() {
        inputCardHolderField.click();
    }

    public void cardHolderFieldSendKeys(Keys inputText) {
        inputCardHolderField.sendKeys(inputText);
    }

    public void cardHolderFieldSendKeys(String inputText) {
        inputCardHolderField.sendKeys(inputText);
    }

    public String getCardHolderFieldErrorText() {
        return cardHolderField.findElement(By.xpath(".//label[@class='error']")).getText();
    }

    /**
     * cardExpired
     */
    public Boolean cardExpiredFieldVisible() {
        return cardExpiredField.isDisplayed();
    }

    public String getCardExpiredFieldErrorText() {
        return cardExpiredField.findElement(By.xpath(".//label[@class='error']")).getText();
    }

    public void cardExpiredMonthFieldClick() {
        cardExpiredMonthField.click();
    }

    public void cardExpiredMonthSelectByVisibleText(String input) {
        getSelect(cardExpiredMonthField).selectByVisibleText(input);
    }

    public void cardExpiredMonthFieldSendKeys(Keys inputText) {
        cardExpiredMonthField.sendKeys(inputText);
    }

    public void cardExpiredMonthYearClick() {
        cardExpiredYearField.click();
    }

    public void cardExpiredYearSelectByVisibleText(String input) {
        getSelect(cardExpiredYearField).selectByVisibleText(input);
    }

    public void cardExpiredYearFieldSendKeys(Keys inputText) {
        cardExpiredYearField.sendKeys(inputText);
    }

    /**
     * cardCVC
     */
    public Boolean cardCVCFieldVisible() {
        return cardExpiredField.isDisplayed();
    }

    public String getCardCVCFieldErrorText() {
        return cardCVCField.findElement(By.xpath(".//label[@class='error']")).getText();
    }

    public void inputCardCVCFieldClick() {
        inputCardCVCField.click();
    }

    public void inputCardCVCFieldSendKeys(Keys inputText) {
        inputCardCVCField.sendKeys(inputText);
    }

    public void inputCardCVCFieldSendKeys(String inputText) {
        inputCardCVCField.sendKeys(inputText);
    }

    /**
     * paymentActions
     */
    public Boolean paymentActionsVisible() {
        return paymentActions.isDisplayed();
    }

    public void payButtonClick() {
        payButton.click();
        waitForJSandJQueryToLoad();
    }

    public void cancelButtonClick() {
        cancelButton.click();
        waitForJSandJQueryToLoad();
    }
}
