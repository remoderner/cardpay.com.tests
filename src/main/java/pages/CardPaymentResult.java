package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CardPaymentResult extends Page {

    public CardPaymentResult(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@id='payment-item-status']/div[@class='payment-info-item-data']")
    private WebElement paymentStatusData; //Payment status data

    @FindBy(xpath = "//div[@id='payment-item-ordernumber']/div[@class='payment-info-item-data']")
    private WebElement orderNumberData; //Order number data

    @FindBy(xpath = "//div[@id='payment-item-authcode']/div[@class='payment-info-item-data']")
    private WebElement authorizationCodeData; //Authorization Code data

    @FindBy(xpath = "//div[@id='payment-item-cardnumber']/div[@class='payment-info-item-data']")
    private WebElement cardNumberData; //Card Number data

    @FindBy(xpath = "//div[@id='payment-item-cardtype']/div[@class='payment-info-item-data']")
    private WebElement cardTypeData; //Card Type data

    @FindBy(xpath = "//div[@id='payment-item-cardholder']/div[@class='payment-info-item-data']")
    private WebElement cardHolderData; //Card Holder data

    @FindBy(xpath = "//div[@id='payment-item-total']/div[@class='payment-info-item-data']")
    private WebElement totalAmountData; //Total Amount data

    public String getPaymentStatusDataText() {
        return paymentStatusData.getText();
    }

    public String getOrderNumberDataText() {
        return orderNumberData.getText();
    }

    public String getAuthorizationCodeDataText() {
        return authorizationCodeData.getText();
    }

    public String getCardNumberDataText() {
        return cardNumberData.getText();
    }

    public String getCardTypeDataText() {
        return cardTypeData.getText();
    }

    public String getCardHolderDataText() {
        return cardHolderData.getText();
    }

    public String getTotalAmountDataText() {
        return totalAmountData.getText();
    }
}
