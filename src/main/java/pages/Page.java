package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

abstract class Page {
    private JavascriptExecutor jScriptExecutor;
    WebDriver driver;

    Page(WebDriver driver) {
        this.driver = driver;
        jScriptExecutor = (JavascriptExecutor) this.driver;
        PageFactory.initElements(driver, this);
    }

    void waitForJSandJQueryToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);

        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = driver -> {
            try {
                return ((Long) (jScriptExecutor.executeScript("return jQuery.active")) == 0);

            } catch (Exception e) {
                // no jQuery present
                return true;
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = driver -> (jScriptExecutor.executeScript("return document.readyState").toString().equals("complete"));

        wait.until(jQueryLoad);
        wait.until(jsLoad);
    }
}
